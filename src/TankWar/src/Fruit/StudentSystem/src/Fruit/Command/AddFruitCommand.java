package Fruit.Command;

import Fruit.Strategy.AddFruit;

public class AddFruitCommand implements Command {
    private AddFruit addFruit;

    public AddFruitCommand(AddFruit addFruit) {
        this.addFruit = addFruit;
    }

    @Override
    public void execute() {
        addFruit.show();
    }
}

