package Fruit.Command;
//创建一个命令接口 Command，定义一个执行命令的方法 execute()
public interface Command {
    void execute();
}
//针对每个具体的菜单操作（添加、删除、修改、查询），
// 我们创建了相应的命令类（AddFruitCommand、DeleteFruitCommand、UpdateFruitCommand、GetallFruitCommand），
// 并实现了Command接口的execute方法。每个命令类持有对应的菜单类的实例，
// 在execute方法中调用对应菜单的show方法执行具体的操作。
// 这样，我们可以通过命令对象来执行菜单操作，实现了命令模式的封装和解耦特性。

