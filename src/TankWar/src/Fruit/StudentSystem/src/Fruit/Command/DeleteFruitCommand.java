package Fruit.Command;

import Fruit.window.DeleteFruit;

public class DeleteFruitCommand implements Command {
    private DeleteFruit deleteFruit;

    public DeleteFruitCommand(DeleteFruit deleteFruit) {
        this.deleteFruit = deleteFruit;
    }

    @Override
    public void execute() {
        deleteFruit.show();
    }
}
