package Fruit.Command;

import Fruit.MVC.GetallFruit;
import Fruit.Strategy.AddFruit;
import Fruit.window.DeleteFruit;
import Fruit.window.UpdateFruit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FruitMenu {
    private JFrame frame;
    private JPanel panel;
    private GridLayout layout;
    private JButton addButton;
    private JButton deleteButton;
    private JButton updateButton;
    private JButton getAllButton;
    private JButton backButton;

    public void show() {
        frame = new JFrame();
        frame.setBounds(400, 100, 500, 300);
        frame.add(createPanel());
        frame.setVisible(true);
    }

    public JPanel createPanel() {
        panel = new JPanel(new FlowLayout());

        addButton = new JButton("添加水果类别");
        panel.add(addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddFruit addFruit = new AddFruit();
                Command command = new AddFruitCommand(addFruit);
                command.execute();
            }
        });

        deleteButton = new JButton("删除水果类别");
        panel.add(deleteButton);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteFruit deleteFruit = new DeleteFruit();
                Command command = new DeleteFruitCommand(deleteFruit);
                command.execute();
            }
        });

        updateButton = new JButton("修改水果类别");
        panel.add(updateButton);
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UpdateFruit updateFruit = new UpdateFruit();
                Command command = new UpdateFruitCommand(updateFruit);
                command.execute();
            }
        });

        getAllButton = new JButton("查询水果类别");
        panel.add(getAllButton);
        getAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GetallFruit getallFruit = new GetallFruit();
                Command command = new GetallFruitCommand(getallFruit);
                command.execute();
            }
        });

        backButton = new JButton("退出");
        panel.add(backButton);
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        return panel;
    }

}
