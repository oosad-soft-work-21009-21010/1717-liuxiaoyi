package Fruit.Command;

import Fruit.MVC.GetallFruit;

public class GetallFruitCommand implements Command {
    private GetallFruit getallFruit;

    public GetallFruitCommand(GetallFruit getallFruit) {
        this.getallFruit = getallFruit;
    }

    @Override
    public void execute() {
        getallFruit.show();
    }
}
