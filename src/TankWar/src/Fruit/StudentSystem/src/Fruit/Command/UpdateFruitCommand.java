package Fruit.Command;

import Fruit.window.UpdateFruit;

public class UpdateFruitCommand implements Command {
    private UpdateFruit updateFruit;

    public UpdateFruitCommand(UpdateFruit updateFruit) {
        this.updateFruit = updateFruit;
    }

    @Override
    public void execute() {
        updateFruit.show();
    }
}