package Fruit.Factory;

//创建具体的菜单项类，如 FruitMenuItem 和 ExitMenuItem，实现 MenuItem 接口
public class ExitMenuItem implements MenuItem {
    public void show() {
        // Add exit logic here
        System.exit(0);
    }
}