package Fruit.Factory;


import Fruit.Command.FruitMenu;

public class FruitMenuItem implements MenuItem {
    public void show() {
        new FruitMenu().show();
    }
}

