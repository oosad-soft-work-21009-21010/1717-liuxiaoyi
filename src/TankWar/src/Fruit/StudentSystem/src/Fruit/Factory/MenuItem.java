package Fruit.Factory;

public interface MenuItem {
    void show();
}
