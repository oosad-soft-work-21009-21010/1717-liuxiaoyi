package Fruit.Factory;
//菜单项工厂类
public class MenuItemFactory {
    public static MenuItem createMenuItem(String type) {
        if (type.equals("fruit")) {
            return new FruitMenuItem();
        } else if (type.equals("exit")) {
            return new ExitMenuItem();
        }
        return null;
    }
}

