package Fruit.Factory;

import Fruit.Singleton.Login;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menue {
    private JFrame jFrame;
    private JPanel jPanel;
    private GridLayout layout;

    private static Menue instance;
    public Menue() {
    }
    public static Menue getInstance() {
        if (instance == null) {
            instance = new Menue();
        }
        return instance;
    }

    public void show() {
        jFrame = new JFrame();
        jFrame.setBounds(200, 100, 200, 150);
        jFrame.add(panel());
        jFrame.setVisible(true);
    }

    public JPanel panel() {
        jPanel = new JPanel(new FlowLayout());

        JButton fruit = new JButton("水果");
        jPanel.add(fruit);
        fruit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MenuItem menuItem = MenuItemFactory.createMenuItem("fruit");
                menuItem.show();
            }
        });

        JButton exit = new JButton("退出");
        jPanel.add(exit);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.setVisible(false); // 隐藏当前菜单窗口
                Login login = new Login();
                login.show(); // 显示登录窗口
            }
        });

        return jPanel;
    }

}
