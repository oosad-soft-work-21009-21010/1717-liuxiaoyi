package Fruit.MVC;

import Fruit.SQL.FruitSalesSystemSQL;

public class FruitController {
    private FruitSalesSystemSQL salesSystem;
    private FruitTable fruitTable;

    public FruitController() {
        salesSystem = new FruitSalesSystemSQL();
    }

    public void updateFruitTable() {
        Object[][] data = salesSystem.getAllFruitsData();
        Object[] header = new Object[]{"ID", "名称", "产地", "价格"};
        fruitTable = new FruitTable(data, header);
    }

    public void showFruitTable() {
        fruitTable.show();
    }
}
