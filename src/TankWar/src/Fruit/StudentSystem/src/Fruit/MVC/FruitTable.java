package Fruit.MVC;

import javax.swing.*;

public class FruitTable {
    private JFrame frame;
    private JTable table;

    public FruitTable(Object[][] data, Object[] header) {
        frame = new JFrame("查询水果");
        frame.setBounds(500, 100, 500, 500);
        table = new JTable(data, header);
        frame.add(new JScrollPane(table));
    }

    public void show() {
        frame.setVisible(true);
    }
}

