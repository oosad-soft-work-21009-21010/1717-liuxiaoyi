package Fruit.MVC;

import Fruit.SQL.FruitSalesSystemSQL;

import javax.swing.*;
import java.util.ArrayList;

public class GetallFruit {
    private JFrame jFrame;
    private JPanel jPanel;
    private Object[] header = new Object[]{"ID", "名称", "产地", "价格"};
    private JTable table;
    private JScrollPane jScrollPane;

    public void show() {
        jFrame = new JFrame("查询水果");
        jFrame.setBounds(500, 100, 500, 500);
        jFrame.add(panel());
        jFrame.setVisible(true);
    }

    public JPanel panel() {
        jPanel = new JPanel(null);
        Object[][] data = new Object[99][4];
        // Populate data with fruit information
        ArrayList<Fruit> fruitList = FruitSalesSystemSQL.getFruitList();
        for (int i = 0; i < fruitList.size(); i++) {
            Fruit fruit = fruitList.get(i);
            data[i][0] = fruit.getId();
            data[i][1] = fruit.getName();
            data[i][2] = fruit.getOrigin();
            data[i][3] = fruit.getPrice();
        }
        table = new JTable(data, header);
        jScrollPane = new JScrollPane(table);
        jScrollPane.setBounds(0, 0, 500, 500);
        jPanel.add(jScrollPane);
        return jPanel;
    }
}
