package Fruit.SQL;

import Fruit.MVC.Fruit;

import java.sql.*;
import java.util.ArrayList;

public class FruitSalesSystemSQL {
    public static Object[][] fruitData = new Object[99][4];

    private static Connection getConn() {
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/FruitSales?serverTimezone=UTC&useCursorFetch=true";
        String username = "root";
        String password = "1234567890";
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 插入水果数据
     */
    public static int insert(Fruit fruit) {
        Connection conn = getConn();
        int i = 0;
        String sql = "INSERT INTO fruit (id, name, price, origin) VALUES (?, ?, ?, ?)";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, fruit.getId());
            pstmt.setString(2, fruit.getName());
            pstmt.setDouble(3, fruit.getPrice());
            pstmt.setString(4, fruit.getOrigin());
            i = pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    /**
     * 修改水果数据
     */
    public static boolean update(int id, double price, String origin) {
        Connection conn = getConn();
        int i = 0;
        String sql = "UPDATE fruit SET price = ?, origin = ? WHERE id = ?";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, price);
            pstmt.setString(2, origin);
            pstmt.setInt(3, id);
            i = pstmt.executeUpdate();
            pstmt.close();
            conn.close();

            if (i > 0) {
                return true; // 更新成功
            } else {
                return false; // 更新失败，ID不存在
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 查询所有水果数据
     */
    public static void getAll() {
        Connection conn = getConn();
        String sql = "SELECT * FROM fruit";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            int col = rs.getMetaData().getColumnCount();
            int i = 0;
            while (rs.next()) {
                fruitData[i][0] = rs.getInt(1);
                fruitData[i][1] = rs.getString(2);
                fruitData[i][2] = rs.getDouble(3);
                fruitData[i][3] = rs.getString(4);
                i++;
            }
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * 删除水果数据
     */
    public static int delete(int id) {
        Connection conn = getConn();
        int i = 0;
        String sql = "DELETE FROM fruit WHERE id = ?";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            i = pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    public static ArrayList<Fruit> getFruitList() {
        ArrayList<Fruit> fruitList = new ArrayList<>();

        Connection conn = getConn();
        String sql = "SELECT * FROM fruit";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String origin = rs.getString("origin");
                double price = rs.getDouble("price");

                Fruit fruit = new Fruit(id, name, price, origin);
                fruitList.add(fruit);
            }
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return fruitList;
    }

    public static Object[][] getAllFruitsData() {
        Connection conn = getConn();
        String sql = "SELECT * FROM fruit";
        Statement stmt;
        ResultSet rs;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);
            rs.last(); // 将光标移动到结果集的最后一行
            int rowCount = rs.getRow(); // 获取结果集的行数
            rs.beforeFirst(); // 将光标重新移动到结果集的开头

            Object[][] data = new Object[rowCount][4];
            int i = 0;
            while (rs.next()) {
                data[i][0] = rs.getInt(1);
                data[i][1] = rs.getString(2);
                data[i][2] = rs.getDouble(3);
                data[i][3] = rs.getString(4);
                i++;
            }

            rs.close();
            stmt.close();
            conn.close();

            return data;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    //相同ID 重复查询
    public static boolean checkIfFruitExists(int id) {
        Connection conn = getConn();
        String sql = "SELECT * FROM fruit WHERE id = ?";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            boolean isExists = rs.next(); // Check if the result set has any rows
            rs.close();
            pstmt.close();
            conn.close();
            return isExists;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}

