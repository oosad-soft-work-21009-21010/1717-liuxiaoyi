package Fruit.Singleton;

import Fruit.Factory.Menue;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class Login {
    private static Login instance; // 单例实例

    private JFrame jFrame;
    private JPanel jPanel;
    private JLabel user;
    private JLabel password;
    private JTextField usertxt;
    private JTextField passwordtxt;
    private JButton login;
    private JButton registered;

    public Login() {
        // 私有构造函数
    }

    public static Login getInstance() {
        if (instance == null) {
            instance = new Login();
        }
        return instance;
    }

    public void show() {
        jFrame = new JFrame("水果销售系统");
        jFrame.setBounds(700, 200, 400, 300);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.add(panel());
        jFrame.setVisible(true);
    }

    public JPanel panel() {
        jPanel = new JPanel(null);

        user = new JLabel("用户名");
        user.setBounds(50, 50, 50, 30);
        jPanel.add(user);

        usertxt = new JTextField();
        usertxt.setBounds(120, 50, 100, 30);
        jPanel.add(usertxt);

        password = new JLabel("密码");
        password.setBounds(50, 100, 50, 30);
        jPanel.add(password);

        passwordtxt = new JTextField();
        passwordtxt.setBounds(120, 100, 100, 30);
        jPanel.add(passwordtxt);

        login = new JButton("登录");
        login.setBounds(80, 150, 70, 30);
        jPanel.add(login);
        login.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usertxt.getText().trim();
                String password = passwordtxt.getText().trim();

                if (username.isEmpty() || password.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "请输入用户名和密码", "登录错误", JOptionPane.ERROR_MESSAGE);
                } else {
//                    并添加一个静态的getInstance()方法，确保只有一个Menue实例被创建。
//                    通过调用Menue.getInstance()方法获取该唯一实例
//                    这样可以确保只有一个主菜单实例被创建，并在整个应用程序中共享该实例
                    Menue.getInstance().show();
                    jFrame.setVisible(false);
                }
            }
        });

        registered = new JButton("注册");
        registered.setBounds(170, 150, 70, 30);
        jPanel.add(registered);
        registered.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
//
                Registered.getInstance().show(); // Show the registration screen
                jFrame.setVisible(false); // Hide the login screen
            }
        });

        return jPanel;
    }
}
