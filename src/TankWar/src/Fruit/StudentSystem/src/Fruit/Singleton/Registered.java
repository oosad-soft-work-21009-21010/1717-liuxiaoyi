package Fruit.Singleton;


import Fruit.Factory.Menue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Registered {
    private JFrame jFrame;
    private JPanel jPanel;
    private JLabel user;
    private JLabel password;
    private JTextField usertxt;
    private JTextField passwordtxt;
    private JButton login;
    private static Registered instance; // 单例实例
    // 私有构造函数，防止外部实例化
    private Registered() {
    }
    public static Registered getInstance() {
        if (instance == null) {
            instance = new Registered();
        }
        return instance;
    }

    public void show() {
        jFrame = new JFrame("管理员");
        jFrame.setBounds(700, 200, 400, 300);
        jFrame.add(panel());
        jFrame.setVisible(true);
    }

    public JPanel panel() {
        jPanel = new JPanel(new BorderLayout(10, 10));
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JPanel formPanel = new JPanel(new GridLayout(2, 2, 10, 10));

        user = new JLabel("用户名");
        formPanel.add(user);

        usertxt = new JTextField();
        formPanel.add(usertxt);

        password = new JLabel("密码");
        formPanel.add(password);

        passwordtxt = new JTextField();
        formPanel.add(passwordtxt);

        jPanel.add(formPanel, BorderLayout.CENTER);

        login = new JButton("登录");
        jPanel.add(login, BorderLayout.SOUTH);
        login.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Menue().show();
                jFrame.setVisible(false);
            }
        });

        return jPanel;
    }
}
