package Fruit.Strategy;

import Fruit.MVC.Fruit;
import Fruit.SQL.FruitSalesSystemSQL;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddFruit {
    private JFrame frame;
    private JPanel panel;
    private JLabel idLabel;
    private JTextField idTextField;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JLabel priceLabel;
    private JTextField priceTextField;
    private JLabel originLabel;
    private JTextField originTextField;
    private JButton addButton;

    private AddFruitValidationStrategy validationStrategy; // 策略接口

    public void show() {
        frame = new JFrame();
        frame.setBounds(700, 100, 400, 400);
        frame.add(createPanel());
        frame.setVisible(true);
    }

    public JPanel createPanel() {
        panel = new JPanel(null);

        idLabel = new JLabel("水果ID");
        idLabel.setBounds(30, 30, 70, 30);
        panel.add(idLabel);

        idTextField = new JTextField();
        idTextField.setBounds(120, 30, 100, 30);
        panel.add(idTextField);

        nameLabel = new JLabel("水果名称");
        nameLabel.setBounds(30, 80, 70, 30);
        panel.add(nameLabel);

        nameTextField = new JTextField();
        nameTextField.setBounds(120, 80, 100, 30);
        panel.add(nameTextField);

        priceLabel = new JLabel("水果价格");
        priceLabel.setBounds(30, 130, 70, 30);
        panel.add(priceLabel);

        priceTextField = new JTextField();
        priceTextField.setBounds(120, 130, 100, 30);
        panel.add(priceTextField);

        originLabel = new JLabel("水果产地");
        originLabel.setBounds(30, 180, 70, 30);
        panel.add(originLabel);

        originTextField = new JTextField();
        originTextField.setBounds(120, 180, 100, 30);
        panel.add(originTextField);

        addButton = new JButton("添加");
        addButton.setBounds(80, 230, 100, 30);
        panel.add(addButton);

        // 设置默认的验证策略
        validationStrategy = new DefaultAddFruitValidationStrategy();

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(idTextField.getText());
                String name = nameTextField.getText();
                double price = Double.parseDouble(priceTextField.getText());
                String origin = originTextField.getText();

                // 使用验证策略进行验证
                if (validationStrategy.validate(id, name, price, origin)) {
                    // 验证通过，创建Fruit对象并插入数据库
                    Fruit fruit = new Fruit(id, name, price, origin);
                    FruitSalesSystemSQL.insert(fruit);
                }
            }
        });

        return panel;
    }

    public void setValidationStrategy(AddFruitValidationStrategy validationStrategy) {
        this.validationStrategy = validationStrategy;
    }
}
