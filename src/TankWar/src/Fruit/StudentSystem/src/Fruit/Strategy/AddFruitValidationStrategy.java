package Fruit.Strategy;

public interface AddFruitValidationStrategy {
    boolean validate(int id, String name, double price, String origin);
}

