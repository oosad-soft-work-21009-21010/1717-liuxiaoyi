package Fruit.Strategy;

public class DefaultAddFruitValidationStrategy implements AddFruitValidationStrategy {
    @Override
    public boolean validate(int id, String name, double price, String origin) {
        if (name.isEmpty() || origin.isEmpty()) {
            return false; // 任意字段为空，验证失败
        }
//        对于id和price字段，我们不再进行空值验证，
//        因为它们是基本数据类型，不可能为空。只对name和origin进行空值验证即可。

        return true; // 所有字段验证通过
    }
}

