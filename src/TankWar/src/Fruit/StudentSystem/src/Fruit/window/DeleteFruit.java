package Fruit.window;

import Fruit.MVC.Fruit;
import Fruit.SQL.FruitSalesSystemSQL;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DeleteFruit {
    private JFrame frame;
    private JPanel panel;
    private JLabel idLabel;
    private JTextField idField;
    private JButton deleteButton;
    private JTextArea fruitInfoTextArea;

    public void show() {
        frame = new JFrame();
        frame.setBounds(700, 100, 400, 400);
        frame.add(createPanel());
        frame.setVisible(true);
    }

    public JPanel createPanel() {
        panel = new JPanel(null);

        idLabel = new JLabel("水果ID");
        idLabel.setBounds(30, 30, 50, 30);
        panel.add(idLabel);

        idField = new JTextField();
        idField.setBounds(100, 30, 100, 30);
        panel.add(idField);

        deleteButton = new JButton("删除");
        deleteButton.setBounds(60, 280, 80, 30);
        panel.add(deleteButton);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(idField.getText());
                int result = FruitSalesSystemSQL.delete(id);
                if (result > 0) {
                    JOptionPane.showMessageDialog(null, "删除成功！");
                    refreshFruitInfo();
                } else {
                    JOptionPane.showMessageDialog(null, "删除失败！");
                }
            }
        });

        fruitInfoTextArea = new JTextArea();
        fruitInfoTextArea.setEditable(false);
        fruitInfoTextArea.setBounds(30, 80, 320, 180);
        panel.add(fruitInfoTextArea);

        refreshFruitInfo();

        return panel;
    }

    private void refreshFruitInfo() {
        fruitInfoTextArea.setText("");
        FruitSalesSystemSQL.getAll();
        ArrayList<Fruit> fruitList = FruitSalesSystemSQL.getFruitList();
        for (Fruit fruit : fruitList) {
            int id = fruit.getId();
            String name = fruit.getName();
            double price = fruit.getPrice();
            String origin = fruit.getOrigin();
            fruitInfoTextArea.append("水果ID：" + id + "\n");
            fruitInfoTextArea.append("水果名称：" + name + "\n");
            fruitInfoTextArea.append("水果价格：" + price + "\n");
            fruitInfoTextArea.append("水果产地：" + origin + "\n");
            fruitInfoTextArea.append("--------------------\n");
        }
    }
}
