package Fruit.window;

import Fruit.SQL.FruitSalesSystemSQL;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UpdateFruit {
    private JFrame jFrame;
    private JPanel jPanel;
    private JLabel id;
    private JTextField idtxt;
    private JLabel price;
    private JTextField pricetxt;
    private JLabel origin;
    private JTextField origintxt;
    private JButton update;

    public void show(){
        jFrame = new JFrame();
        jFrame.setBounds(700,100,400,400);
        jFrame.add(panel());
        jFrame.setVisible(true);
    }

    public JPanel panel(){
        jPanel = new JPanel(null);

        id = new JLabel("ID");
        id.setBounds(30,30,50,30);
        jPanel.add(id);

        idtxt = new JTextField();
        idtxt.setBounds(100,30,100,30);
        jPanel.add(idtxt);


        price = new JLabel("价格");
        price.setBounds(30,180,50,30);
        jPanel.add(price);

        pricetxt = new JTextField();
        pricetxt.setBounds(100,180,100,30);
        jPanel.add(pricetxt);

        origin = new JLabel("产地");
        origin.setBounds(30,230,50,30);
        jPanel.add(origin);

        origintxt = new JTextField();
        origintxt.setBounds(100,230,100,30);
        jPanel.add(origintxt);

        update = new JButton("修改");
        update.setBounds(60,280,80,30);
        jPanel.add(update);
        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(idtxt.getText());
                String origin = origintxt.getText();
                double price = Double.parseDouble(pricetxt.getText());
                boolean success = FruitSalesSystemSQL.update(id, price, origin);
                if (success) {
                    JOptionPane.showMessageDialog(null, "修改成功");
                } else {
                    JOptionPane.showMessageDialog(null, "修改失败，ID不存在");
                }
            }
        });

        return jPanel;
    }
}