package t14;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class TankClient14 extends Frame {
    public static final int GAME_WIDTH = 800;//宽度常量
    public static final int GAME_HEIGHT = 600;//高度常量
    Tank myTank = new Tank(50, 50,this);
    List<Missile> missiles = new ArrayList<Missile>();
    Missile m = new Missile(50, 50, Tank.Direction.R);
    Image offScreenImage = null;//虚拟图片

    public void launchFrame() {
        //设置窗口出现的位置
        this.setLocation(300, 50);
        //设置窗口的宽度高度
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setBackground(Color.green);
        this.setTitle("TankWar");
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setResizable(false);
        setVisible(true);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        new Thread(new PaintThread()).start();
    }

    //paint这个方法不需要被调用，一旦要被重画的时会被自动 调用
    public void paint(Graphics g) {
        //看出容器中装了多少炮弹
        g.drawString("missiles count: " + missiles.size(), 10, 50);
        //将容器中的炮弹逐个画出来
        for(int i = 0; i < missiles.size(); i++) {
            Missile m = missiles.get(i);
            m.draw(g);
            if(!m.isLive()) {
                missiles.remove(m);
            } else {
                m.draw(g);
            }
        }
        myTank.draw(g);
    }

    private class KeyMonitor extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            myTank.KyePressed(e);
        }

        public void keyReleased(KeyEvent e) {
            myTank.kyeReleased(e);
        }
    }

    private class PaintThread implements Runnable {
        //内部类（a)可以方便的访问包装类的方法。不方便公开的，只为包装类服务的类应当定义为内部类）
        @Override
        public void run() {
            while (true) {
                repaint();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void update(Graphics g) {
        if (offScreenImage == null) {    //若为null则创建一张图
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        //拿到这个图片的画笔
        Graphics gOffScreen = offScreenImage.getGraphics();
        //默认黑色，所以需要与效果背景一致获取原色
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);//与原背景色一致
        //使用画笔画出一个实现的方框代替原画的背景效果
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);//设置原回来
        //使用虚拟图像的画笔画圆
        paint(gOffScreen);
        //将虚拟图片画下来
        g.drawImage(offScreenImage, 0, 0, null);
    }


    public static void main(String[] args) {
        TankClient14 tc = new TankClient14();
        tc.launchFrame();

    }

}