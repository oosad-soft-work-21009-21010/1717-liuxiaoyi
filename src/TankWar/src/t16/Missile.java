package t16;

import java.awt.*;


public class Missile {
    public static final int XSPEED = 10;
    public static final int YSPEED = 10;

    public static final int WIDTH = 10;
    public static final int HEIGHT = 10;

    private int x, y;
    Tank.Direction dir;
    //判断子弹是否活着
    private boolean live = true;

    public boolean isLive() {
        return live;
    }

    private TankClient16 tc;

    public Missile(int x, int y, Tank.Direction dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
    }

    public Missile(int x, int y, Tank.Direction dir, TankClient16 tc) {
        this(x, y, dir);
        this.tc = tc;
    }

    public void draw(Graphics g) {
//如果没有活着，就从容器中去掉，并且不再画了
        if (!live) {
            tc.missiles.remove(this);
            return;
        }
        Color c = g.getColor();
        g.setColor(Color.BLACK);
        g.fillOval(x, y, WIDTH, HEIGHT);
        g.setColor(c);

        move();
    }

    private void move() {
        switch (dir) {
            case L:
                x -= XSPEED;
                break;
            case LU:
                x -= XSPEED;
                y -= YSPEED;
                break;
            case U:
                y -= YSPEED;
                break;
            case RU:
                x += XSPEED;
                y -= YSPEED;
                break;
            case R:
                x += XSPEED;
                break;
            case RD:
                x += XSPEED;
                y += YSPEED;
                break;
            case D:
                y += YSPEED;
                break;
            case LD:
                x -= XSPEED;
                y += YSPEED;
                break;
        }
        if (x < 0 || y < 0 || x > TankClient16.GAME_WIDTH || y > TankClient16.GAME_HEIGHT) {
            live = false;
            tc.missiles.remove(this);
        }
    }


    //getRect()可以正好拿到包在坦克外面的方块
    public Rectangle getRect() {
        return new Rectangle(x, y, WIDTH, HEIGHT);
    }

    //intersects可以判断这两个方块是否相交
//如果已经碰撞，那么就将子弹和坦克都设置为false
    public boolean hitTank(Tank t) {
        if (this.getRect().intersects(t.getRect()) &&
                t.isLive()) {
            t.setLive(false);
            this.live = false;
            return true;
        }
        return false;
    }
}