package t24;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class TankClient24 extends Frame {
    public static final int GAME_WIDTH = 800;//宽度常量
    public static final int GAME_HEIGHT = 600;//高度常量
    Tank myTank = new Tank(50, 50,true,this);
    // Tank enemyTank=new Tank(100,100,false,this);
    List<Missile> missiles = new ArrayList<Missile>();

    List<Explode> explodes = new ArrayList<Explode>();
    List<Tank> tanks = new ArrayList<Tank>();
    Wall w1=new Wall(230,250,250,10,this);
    //Wall w2=new Wall(250,250,250,10,this);
    Missile m = new Missile(50, 50, Tank.Direction.R);
    Image offScreenImage = null;//虚拟图片

    public void launchFrame() {
        for(int i = 0; i < 10; i++) {
            tanks.add(new Tank(50 + 40 * (i + 1), 50, false, this));
            tanks.get(i).collidesWithWall(w1);
            // tanks.get(i).collidesWithWall(w2);
        }
        //设置窗口出现的位置
        this.setLocation(300, 50);
        //设置窗口的宽度高度
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setBackground(Color.green);
        this.setTitle("TankWar");
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setResizable(false);
        setVisible(true);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        new Thread(new PaintThread()).start();
    }

    //paint这个方法不需要被调用，一旦要被重画的时会被自动 调用
    public void paint(Graphics g) {
        //看出容器中装了多少炮弹
        g.drawString("missiles count: " + missiles.size(), 10, 50);
        //显示有多少个爆炸
        g.drawString("explodes count: " + explodes.size(), 10,70);
        g.drawString("tanks count: " + tanks.size(), 10, 90);
        g.drawString("blood: " + myTank.getLife(), 10, 110);


        //将容器中的炮弹逐个画出来
        for(int i = 0; i < explodes.size(); i++) {
            Explode e = explodes.get(i);
            e.draw(g);
        }
        w1.draw(g);
        // w2.draw(g);
        myTank.draw(g);

        myTank.collidesWithWall(w1);
        myTank.collidesWithTanks(tanks);

        for (int i=0;i<tanks.size();i++){
            tanks.get(i).draw(g);
            tanks.get(i).collidesWithWall(w1);
            tanks.get(i).collidesWithTanks(tanks);
        }
        //将容器中的爆炸逐个画出来
        for(int i = 0; i < missiles.size(); i++) {
            Missile m=missiles.get(i);
            m.hitTanks(tanks);
            m.hitTank(myTank);
            m.hitWall(w1);
            //  m.hitWall(w2);
            m.draw(g);
        }

    }

    private class KeyMonitor extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            myTank.KyePressed(e);
        }

        public void keyReleased(KeyEvent e) {
            myTank.kyeReleased(e);
        }
    }

    private class PaintThread implements Runnable {
        //内部类（a)可以方便的访问包装类的方法。不方便公开的，只为包装类服务的类应当定义为内部类）
        @Override
        public void run() {
            while (true) {
                repaint();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void update(Graphics g) {
        if (offScreenImage == null) {    //若为null则创建一张图
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        //拿到这个图片的画笔
        Graphics gOffScreen = offScreenImage.getGraphics();
        //默认黑色，所以需要与效果背景一致获取原色
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);//与原背景色一致
        //使用画笔画出一个实现的方框代替原画的背景效果
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);//设置原回来
        //使用虚拟图像的画笔画圆
        paint(gOffScreen);
        //将虚拟图片画下来
        g.drawImage(offScreenImage, 0, 0, null);
    }


    public static void main(String[] args) {
        TankClient24 tc = new TankClient24();
        tc.launchFrame();

    }

}