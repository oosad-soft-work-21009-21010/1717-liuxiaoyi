package t25;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Tank {
    public static final int XSPEED = 5;
    public static final int YSPEED = 5;
    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;
    //保留TankClient的引用，更方便的使用其中的成员变量
    TankClient25 tc = null;
    int step;

    public void setLife(int life) {
        this.life = life;
    }

    private int life = 100;

    public int getLife() {
        return life;
    }

    private static Random r = new Random();
    Direction[] dirs = Direction.values();
    //将这个枚举类型转为数据组
    int rn = r.nextInt(dirs.length);
    //如果 dirs 的长度为 9，则 r.nextInt 产生 0 到 8 的随机的整数

    private int x, y;

    private Direction ptDir = Direction.D;
    private boolean good;
    private boolean live = true;
    private int oldX;
    private int oldY;

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    //是否按下了4个方向键
    private boolean bL = false,
            bU = false,
            bR = false,
            bD = false;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isGood() {
        return good;
    }


    //成员变量：方向
    enum Direction {L, LU, U, RU, R, RD, D, LD, STOP}

    ;
    private Direction dir = Direction.STOP;

    public Tank(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Tank(int x, int y, boolean good) {
        this.x = x;
        this.y = y;
        this.good = good;
    }

    public Tank(int x, int y, boolean good, TankClient25 tankClient) {
        //调用其他构造方法
        this(x, y, good);
        this.tc = tankClient;

    }
    //在 Tank.java 中增加一个吃血条的方法：
    public boolean eat(Blood b) {
        if(this.live && b.isLive() &&
                this.getRect().intersects(b.getRect())) { this.life = 100; b.setLive(false);
            return true;
        }
        return false;
    }

    public boolean collidesWithTanks(java.util.List<Tank> tanks) {
        for (int i = 0; i < tanks.size(); i++) {
            Tank t = tanks.get(i);
            if (this != t) {
                if (this.live && t.isLive() &&
                        this.getRect().intersects(t.getRect())) {
                    t.stay();
                    this.stay();
                    return true;
                }
            }
        }
        return false;
    }

    public boolean collidesWithWall(Wall w) {
        if (this.getRect().intersects(w.getRect()) &&
                this.live) {
            this.stay();
            return true;
        }
        return false;
    }

    private void stay() {
        x = oldX;
        y = oldY;
    }

    private BloodBar bb=new BloodBar();

    public void draw(Graphics g) {
        if (!live) return;
        if (good) {
            bb.draw(g);
        }
        Color c = g.getColor();
        //以不同的颜色来显示不同的坦克
        if (good) {
            g.setColor(Color.RED);
        } else {
            g.setColor(Color.BLUE);
        }
        g.fillOval(x, y, WIDTH, HEIGHT);
        g.setColor(c);
        move();

        switch (ptDir) {
            case L:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x, y + Tank.HEIGHT / 2);
                break;
            case LU:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x, y);
                break;
            case U:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH / 2, y);
                break;
            case RU:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH, y);
                break;
            case R:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH, y + Tank.HEIGHT / 2);
                break;
            case RD:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH, y + Tank.HEIGHT);
                break;
            case D:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH / 2, y + Tank.HEIGHT);
                break;
            case LD:
                g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x, y + Tank.HEIGHT);
                break;
            case STOP:
                break;
        }

    }

    public Rectangle getRect() {
        return new Rectangle(x, y, WIDTH, HEIGHT);
    }

    void move() {
        this.oldX = x;
        this.oldY = y;
        if (!good) {
//Direction.values()将这个枚举类型转为数组Direction[] dirs = Direction.values();
            if (step == 0) {
                step = r.nextInt(12) + 3;
                int rn = r.nextInt(dirs.length);
                dir = dirs[rn];
            }
            step--;

            if (r.nextInt(40) > 38) this.fire();
        }
        switch (dir) {
            case L:
                x -= XSPEED;
                break;
            case LU:
                x -= XSPEED;
                y -= YSPEED;
                break;
            case U:
                y -= YSPEED;
                break;
            case RU:
                x += XSPEED;
                y -= YSPEED;
                break;
            case R:
                x += XSPEED;
                break;
            case RD:
                x += XSPEED;
                y += YSPEED;
                break;
            case D:
                y += YSPEED;
                break;
            case LD:
                x -= XSPEED;
                y += YSPEED;
                break;
            case STOP:
                break;
        }
        //将坦克方向传给炮筒，使炮筒与坦克方向一致
        if (this.dir != Direction.STOP) {
            this.ptDir = this.dir;
        }
        if (x < 0) x = 0;
        if (y < 25) y = 25;
        if (x + Tank.WIDTH > TankClient25.GAME_WIDTH) x = TankClient25.GAME_WIDTH - Tank.WIDTH;
        if (y + Tank.HEIGHT > TankClient25.GAME_HEIGHT) y = TankClient25.GAME_HEIGHT - Tank.HEIGHT;


    }

    public void KyePressed(KeyEvent e) {
        int key = e.getKeyCode();
        switch (key) {
            //按下Ctrl做的动作
            case KeyEvent.VK_CONTROL:
                tc.m = fire();
                break;
            case KeyEvent.VK_LEFT:
                bL = true;
                break;
            case KeyEvent.VK_UP:
                bU = true;
                break;
            case KeyEvent.VK_RIGHT:
                bR = true;
                break;
            case KeyEvent.VK_DOWN:
                bD = true;
                break;

        }
        locateDirection();

    }

    public Missile fire() {
        if (!live) return null;
        //保证子弹从Tank的中间出现
        int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;
        int y = this.y + Tank.HEIGHT / 2 - Missile.WIDTH / 2;
        //将Tank现在的位置和方向传递给子弹
        //并且现在子弹的初始化不再是坦克决定，而是由炮筒决定
        Missile m = new Missile(x, y, ptDir, tc, this.good);
        //在这里将missiles加入到容器里
        tc.missiles.add(m);
        return m;
    }

    public Missile fire(Direction dir) {
        if (!live) return null;
        //保证子弹从Tank的中间出现
        int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;
        int y = this.y + Tank.HEIGHT / 2 - Missile.WIDTH / 2;
        //将Tank现在的位置和方向传递给子弹
        //并且现在子弹的初始化不再是由坦克决定，而是由炮筒决定
        Missile m = new Missile(x, y, dir, this.tc, good);
        tc.missiles.add(m);
        return m;
    }

    private void superFire() {
        Direction[] dirs = Direction.values();
        for (int i = 0; i < 8; i++) {
            fire(dirs[i]);
        }
    }

    public void kyeReleased(KeyEvent e) {
        int key = e.getKeyCode();
        switch (key) {
            //按下Ctrl时做出动作
            //只有当键被抬起时，这枚炮弹才被发出去
            //避免了一直按下Ctrl而使得子弹过多的问题
            case KeyEvent.VK_CONTROL:
                fire();
                break;
            case KeyEvent.VK_LEFT:
                bL = false;
                break;
            case KeyEvent.VK_UP:
                bU = false;
                break;
            case KeyEvent.VK_RIGHT:
                bR = false;
                break;
            case KeyEvent.VK_DOWN:
                bD = false;
                break;
            case KeyEvent.VK_A:
                superFire();
                break;
        }
        locateDirection();
    }

    private void locateDirection() {
        if (bL && !bU && !bR && !bD)
            dir = Direction.L;
        else if (bL && bU && !bR && !bD)
            dir = Direction.LU;
        else if (!bL && bU && !bR && !bD)
            dir = Direction.U;
        else if (!bL && bU && bR && !bD)
            dir = Direction.RU;
        else if (!bL && !bU && bR && !bD)
            dir = Direction.R;
        else if (!bL && !bU && bR && bD)
            dir = Direction.RD;
        else if (!bL && !bU && !bR && bD)
            dir = Direction.D;
        else if (bL && !bU && !bR && bD)
            dir = Direction.LD;
        else if (!bL && !bU && !bR && !bD)
            dir = Direction.STOP;

    }

    private class BloodBar {
        public void draw(Graphics g) {
            Color c = g.getColor();
            g.setColor(Color.RED);
            g.drawRect(x, y - 10, WIDTH, 10);
            int w = WIDTH * life / 100;
            g.fillRect(x, y - 10, w, 10);
            g.setColor(c);
        }
    }
}


