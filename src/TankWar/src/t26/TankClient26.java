package t26;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.JLabel;
import java.util.ArrayList;
public class TankClient26 extends Frame {
    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 600;
    Tank myTank = new Tank(300,300, true,Tank.Direction.STOP, this);
    Wall w1 = new Wall(100,200,20,150,this);
    Wall w2 = new Wall(300,100,300,20,this);
    Blood b = new Blood();
    // Tank enemyTank = new Tank(100, 100, false,Tank.Direction.STOP, this);
    List<Missile> missiles = new ArrayList<Missile>();
    List<Explode> explodes = new ArrayList<Explode>();
    List<Tank> tanks = new ArrayList<Tank>();
    Image offScreenImage = null;
    int win;
    public void paint(Graphics g) {
        g.drawString("missiles count: " + missiles.size(), 10, 50);
        g.drawString("explodes count: " + explodes.size(), 10, 70);
        g.drawString("tanks count: " + tanks.size(), 10, 90);
        g.drawString("tank life: " + myTank.getLife(), 10, 110);
        if(tanks.size() <= 0 && this.win!=1) {
            for (int i = 0; i < 5; i++) {
                tanks.add(new Tank(50 + 40 * (i + 1), 50, false, Tank.Direction.D,
                        this));
            }
            win=1;
        }
//我方坦克击杀�?有敌方时
        for(int i = 0; i<missiles.size();i++) {
            Missile m =missiles.get(i);
            m.collidesWithTanks(tanks);
            m.collidesWithTank(myTank);
            m.collidesWithWall(w1);
            m.collidesWithWall(w2);
            m.draw(g);
        }
        for(int i = 0; i < explodes.size(); i++) {
            Explode e = explodes.get(i);
            e.draw(g);
        }
        for(int i = 0;i< tanks.size();i++) {
            Tank t =tanks.get(i);
            t.collidesWithWall(w1);
            t.collidesWithWall(w2);
            t.collidesWithTanks(tanks);
            t.draw(g);
        }
        myTank.draw(g);
        myTank.eat(b);
        w1.draw(g);
        w2.draw(g);
        myTank.collidesWithTanks(tanks);
        b.draw(g);
        if(tanks.size() <= 0) {
            g.setColor(Color.RED);
            g.setFont(new Font("Didot", Font.BOLD, 60));
            g.drawString("胜利!", 100, 300);

        }
        if (!myTank.isLive()){
            g.setColor(Color.pink);
            g.setFont(new Font("Didot", Font.BOLD, 40));
            g.drawString("You failed, press A to start again.", 100, 300);
        }
    }
    public void update(Graphics g){
        if(offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH,GAME_HEIGHT);
        }
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        print(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);
    }
    public void launchFrame() {
        this.setLocation(300, 50);
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setTitle("TankWar");
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        this.setResizable(false);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        setVisible(true);
        for(int i=0; i<10;i++) {
            tanks.add(new Tank(50 + 40 * (i+1),50,
                    false,Tank.Direction.D,this));
        }
        new Thread(new PaintThread()).start();
    }
    public static void main(String[] args) {
        TankClient26 tc = new TankClient26();
        tc.launchFrame();
    }
    public class PaintThread implements Runnable {
        public void run() {
            while(true) {
                repaint();
                try {
                    Thread.sleep(50);
                }catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public class KeyMonitor extends KeyAdapter{
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_A) {
                if (!myTank.isLive()) {
                    restart();
                }
            }
            myTank.KyePressed(e);
        }
        public void keyReleased(KeyEvent e) {
            myTank.kyeReleased(e);
        }
    }
    private void restart() {
        myTank.setLive(false);
        myTank = new Tank(50, 50, true, Tank.Direction.STOP, this);
        tanks.clear();
        for (int i = 0; i < 10; i++) {
            tanks.add(new Tank(50 + 40 * (i + 1), 350, false,Tank.Direction.STOP,
                    this));
        }
        missiles.clear();
        explodes.clear();
        b.setLive(false);
        b = new Blood();
    }
}
