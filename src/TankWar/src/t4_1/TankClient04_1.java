package t4_1;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/* 双缓冲工作原理：先启动一个线程，调用 repaint()方法，repaint 会自己调用 update，
   update 会再调用 paint，在系统试用 paint 之前，我们重写 update 方法。
   在 update方法里，先 new 出来一张虚拟的图片，得到这个图片的画笔，
    Graphics gOffScreen = offScreenImage.getGraphics();，
    保存画笔的颜色，设置这支画笔的颜色与游戏背景颜色相同，
    使用这支画笔画一个与游戏窗口大小相同的虚拟图片 ，
    再还原原来的颜色。将这支画笔传给 paint 方法print(gOffScreen);，
    这样 paint方法画出来的画就在这张虚拟图片上。
    最后使用游戏窗口的这支画笔 g 将这张虚拟图片画到游戏窗口中。
   */
public class TankClient04_1 extends Frame {//使用双缓冲消除闪烁(线程sleep时间短会出现闪烁现象)

    int x = 50, y = 50;
    Image offScreenImage = null;

    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.RED);
        g.fillOval(x, y, 30, 30);
        g.setColor(c);
        y += 5;
    }

    public void launchFrame() {
        this.setLocation(300, 50);
        this.setSize(800, 600);
        this.setBackground(Color.green);
        this.setTitle("TankWar");
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setResizable(false);
        setVisible(true);

        new Thread(new PaintThread()).start();
    }

    private class PaintThread implements Runnable {
        //内部类 （可以方便的访问包装类的方法。不方便公开的，只为包装类服务的类应当定义为内部类）
        @Override
        public void run() {
            while (true) {
                repaint();//会自动调用update方法
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public  void update(Graphics g){
        if(offScreenImage==null){
            offScreenImage=this.createImage(800,600);
        }//拿到这个图片的颜色
        Graphics gOffScreen=offScreenImage.getGraphics();//得到这个图片的画笔
        Color c=gOffScreen.getColor();
        gOffScreen.setColor(Color.green);
        gOffScreen.fillRect(0,0,800,600);
        gOffScreen.setColor(c);
        print(gOffScreen);
        g.drawImage(offScreenImage,0,0,null);
    }

    public static void main(String[] args) {
        TankClient04_1 tc=new TankClient04_1();
        tc.launchFrame();
    }
}
